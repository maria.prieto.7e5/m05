package cat.itb.m05.uf2

import java.time.Year
import java.util.*

val scanner = Scanner(System.`in`).useLocale(Locale.UK)


fun welcome(assistantName: String):String {
    return "Hello! My name is $assistantName"
}

fun ageCalculator(yearBorn: Int): List<Int> {
    val currentYear = Year.now().value
    val ageMax = currentYear - yearBorn
    val ageMin = ageMax - 1
    return listOf(ageMin,ageMax)
}

fun imcCalculator(weight : Double,height:Double):Double {
    val imc = weight / (height * height)
    return imc

}

fun trainingSchedule(hours: Int, days: Int): List<List<Int>>{
    println("I'll tell you your training plan.")
    println("How many hours would you like to train?")
    val minHoursPerDay = hours / days
    val maxHoursPerDay = minHoursPerDay + 1
    val daysMaxHours = hours % days
    val daysMinHours = days - daysMaxHours
    return listOf(listOf(2,1), listOf(1,3))

}