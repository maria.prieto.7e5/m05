package cat.itb.m05.uf2

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class TrainingAssistantKtTest {

    @Test
    fun welcomeTest() {
        val expected="Hello! My name is Maria del Mar"
        assertEquals(expected,welcome("Maria del Mar"))
    }

    @Test
    fun ageCalculatorTest() {
        val expected=listOf(20,21)
        assertEquals(expected, ageCalculator(2001))
    }

    @Test
    fun imcCalculatorTest() {
        val expected=22.948115744558788
        assertEquals(expected,imcCalculator(64.4,1.67))
    }

    @Test
    fun trainingScheduleTest() {
        val expected=listOf(listOf(2,1), listOf(1,3))
        assertEquals(expected, trainingSchedule(4,3))
    }
}